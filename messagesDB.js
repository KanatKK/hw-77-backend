const fs = require("fs");

const messageName = "./db.json";
let data = [];
const lastMessages = [];

module.exports = {
    init() {
        try {
            data = JSON.parse(fs.readFileSync(messageName));
        } catch (e) {
            data = [];
        }
    },
    getMessages() {
        lastMessages.length = 0;
        if (data.length < 31) {
            return data
        } else  {
            for (let i = data.length - 30; i <= data.length - 1; i++) {
                if (data[i].message === "" || data[i].author === "") {
                    data[i] = {message: "Author and message must be present in the request"};
                } else {
                    lastMessages.push(data[i]);
                }
            }
            return lastMessages;
        }
    },
    save() {
        fs.writeFileSync(messageName, JSON.stringify(data));
    },
    addMessage(message) {
        message.id = Date.now();
        message.datetime = (new Date()).toISOString();
        if (message.author === "") {
            message.author = "Anonymous"
        }
        data.push(message);
        this.save();
        return message;
    },
};