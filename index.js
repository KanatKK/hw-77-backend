const express = require("express");
const messages = require("./app/messages");
const cors = require("cors");
const db = require("./messagesDB");
const app = express();

db.init();

app.use(cors());
app.use(express.json());
app.use(express.static("public"));
app.use("/messages", messages);

app.listen(8000, () => {});