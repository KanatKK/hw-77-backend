const router = require("express").Router();
const db = require("../messagesDB");
const multer = require("multer");
const path = require("path");
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

router.get("/", (req, res) => {
    const messages = db.getMessages();
    const dateTimeMessages = [];
    if (req.query.datetime !== undefined) {
        if (isNaN(new Date(req.query.datetime.toString())) === true) {
            res.send({error: 'Invalid date'});
        } else {
            for (let i = 0; i <= messages.length - 1; i++) {
                if (messages[i].datetime === req.query.datetime) {
                    for (let k = i + 1; k <= messages.length - 1; k++) {
                        dateTimeMessages.push(messages[k]);
                    }
                }
            }
            res.send(dateTimeMessages);
        }
    } else {
        res.send(messages);
    }
});
router.post("/", upload.single("image") , (req, res) => {
    const message = req.body
    if (req.file) {
        message.image = req.file.filename;
    }
    const response = db.addMessage(message);
    res.send(response);
});

module.exports = router;